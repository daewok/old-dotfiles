;;; init.el --- Emacs init file, based on MERS base config.

;;; Commentary:

;;; This is etimmons Emacs config.  This config uses Emacs' package
;;; manager (package.el) and the MELPA package repo (GNU ELPA is also
;;; enabled by default).  The first time you use this config on a
;;; computer it will take some time to download the pacakge archive
;;; and install all the packages.  In order to upgrade your packages
;;; (recommended you do this at least monthly), run `M-x
;;; package-list-packages', press `U' (for upgrade), and then `x' (for
;;; eXecute).

;;; If you have code that should override anything found in MELPA,
;;; place it in the site-lisp folder

;;; In addition to using the package manager, this config uses
;;; use-package.el to auto install and configure each package.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; External Dependencies

;;; In order to fully utilize some features, external programs may
;;; need to be installed.  We collect those here (except for Lisp
;;; related things, which is on the group wiki).

;;; + Markdown mode requires a Markdown compiler to preview the HTML.
;;; The defualt is currently markdown2
;;; (https://github.com/trentm/python-markdown2), but it can be
;;; customized.

;;; + Flycheck requires some external programs to do checking.  These
;;; are some recommended programs (`flycheck-verify-setup' is still
;;; the definitive source, though).
;;;
;;;   + Python: Flake8 https://bitbucket.org/tarek/flake8


;;; Code:

;; Set the GC threshold really high during init loading.
(defvar my-original-gc-cons-threshold gc-cons-threshold)
(setq gc-cons-threshold 16000000)

;; Start by configuring the package manager and use-package.
;; Everything else will be using them extensively.

(require 'package)

;; MELPA is probably the most complete package repo out there for
;; emacs. Add it to the list of repos for package.el to use. This adds
;; the "unstable" MELPA repo. In practice it tends to be pretty
;; stable, but since it builds from a git branch it's very possible
;; for someone to accidentally push broken code. If you want to be
;; extra careful (but miss out on new features), add stable.melpa.org
;; instead.

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; Init package.el
(package-initialize)

;; If we haven't downloaded any package listing before (i.e., this is
;; the first time we've started Emacs), get the latest package
;; listing.

(when (not package-archive-contents)
  (package-refresh-contents))

;; Now, make sure use-package.el is installed.
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(when (< emacs-major-version 25)
  ;; Sometimes, TRAMP can get stuck loading when the DNS servers
  ;; you're using don't behave quite right. This should be fixed in
  ;; Emacs 25. This needs to be done as early as possible in case any
  ;; packages require tramp.
  (setq tramp-ssh-controlmaster-options ""))

;; Make anything you put in site-lisp take precedence over things from
;; package.el.
(let ((default-directory  "~/.emacs.d/site-lisp/"))
  (setq load-path
        (append
         (let ((load-path  (copy-sequence load-path))) ;; Shadow
           (normal-top-level-add-subdirs-to-load-path))
         load-path)))

(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)

;; Use the dark version of the color theme.
(set-frame-parameter nil 'background-mode 'dark)
;;(set-terminal-parameter nil 'background-mode 'dark)
(setq frame-background-mode 'dark)

;; Set the theme
(use-package color-theme
  :ensure t)
(use-package color-theme-solarized
  :ensure t)
(load-theme 'solarized t)

;; Make the scroll bars a little narrower than the default.
(setq initial-frame-alist '((scroll-bar-width . 15)))


;;; General configuration. The packages here tend to have behavior
;;; that isn't constrained to a particular language or major mode.

(setenv "ZSH_TMUX_AUTOSTART" "false")

;; For our OSX friends, get sane PATH values.
(use-package exec-path-from-shell
  :ensure t
  :config
  (setq exec-path-from-shell-variables
        '("PATH" "MANPATH"))
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)))

;; which-key.el displays a help buffer showing what keys you can press
;; after a short delay.
(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (which-key-mode))

;; ;; neotree.el shows a file browser when F8 is pressed.
;; (use-package neotree
;;   :ensure t
;;   :bind (([f8] . neotree-toggle))
;;   :config
;;   (setq neo-smart-open t
;;         neo-vc-integration '(face char)
;; 	neo-theme 'ascii))

;; ;; sr-speedbar is like neotree with a file tree, but it only shows
;; ;; specific file types. Namely, it shows any file that it knows how to
;; ;; parse information from about functions and types defined in the
;; ;; file.
;; (use-package sr-speedbar
;;   :ensure t
;;   :bind (("s-s" . sr-speedbar-toggle))
;;   :config
;;   (setq sr-speedbar-right-side nil)
;;   (speedbar-add-supported-extension ".lisp"))

;; avy.el is used to quickly jump around the buffer using a tree to
;; select where you want to go.
(use-package avy
  :ensure t
  :bind (("M-g w" . avy-goto-word-1)
         ("M-g g" . avy-goto-line)))

;; ace-window.el replaces the standard C-x o (other-window) so that if
;; multiple windows are open, you can jump to a specific one instead
;; of needing to rotate through them all. It also lets you delete
;; windows by prefixing `ace-window'.
(use-package ace-window
  :ensure t
  :bind (("C-x o" . ace-window)))

;; Use helm.el. Helm is awesome.
(use-package helm
  :ensure t
  :demand t
  :diminish helm-mode
  :config
  (setq helm-M-x-fuzzy-match t
        helm-ff-skip-boring-files t
        helm-buffers-fuzzy-matching t
        helm-recentf-fuzzy-match t)
  (helm-mode 1)
  (add-hook 'helm-major-mode-hook (lambda () (setq display-line-numbers nil)))
  :bind (("C-x C-f" . helm-find-files)
         ("M-x" . helm-M-x)
         ("M-y" . helm-show-kill-ring)
         ("C-x b" . helm-mini)
         ("C-x C-b" . helm-mini)
         ("C-c h o" . helm-occur)))

;; A clone of spacemac's modeline
(use-package spaceline-config
  :ensure spaceline
  :config
  (spaceline-define-segment tracking-mode
    "show names of tracked buffers with changes."
    tracking-mode-line-buffers
    :separator "")
  (spaceline-emacs-theme 'tracking-mode)
  (spaceline-helm-mode 1))

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)
         ("C-r" . swiper)))

;; Make scrolling a little smoother.
(use-package smooth-scrolling
  :ensure t
  :config
  (smooth-scrolling-mode 1))

(use-package beacon
  :ensure t
  :diminish t
  :config
  (beacon-mode 1)
  (add-hook 'slime-repl-mode-hook (lambda () (beacon-mode -1))))

;; (use-package sublimity
;;   :ensure t
;;   :diminish t
;;   :config
;;   (require 'sublimity-scroll)
;;   (sublimity-mode 1))

;; Use editorconfig to allow per-project customization of tabs
;; vs. spaces, etc.
(use-package editorconfig
  :ensure t
  :diminish editorconfig-mode
  :config
  (editorconfig-mode 1))

;; Highlight common "todo" tokens in comment strings.
(use-package hl-todo
  :ensure t
  :config
  (setq hl-todo-activate-in-modes '(prog-mode))
  (global-hl-todo-mode 1))

;; Pretty print ^L characters (page breaks).
(use-package form-feed
  :ensure t
  :diminish form-feed-mode
  :commands (form-feed-mode)
  :init
  (add-hook 'prog-mode-hook 'form-feed-mode))

;; Projectile is an awesome framework for managing projects in
;; emacs. It is highly recommended that you read the docs on it,
;; because there is too much to talk about in a comment
;; (http://projectile.readthedocs.io/en/latest/). The commands you
;; will probably use the most from it are:
;;
;; C-c p p - Jump to project.
;; C-c p f - Find file in project
;; C-c p s g - grep all files in project.
(use-package projectile
  :ensure t
  :diminish projectile-mode
  :config
  (setq projectile-globally-ignored-file-suffixes '("fasl"))
  (projectile-global-mode))

(add-hook 'comint-mode-hook (lambda () (setq display-line-numbers nil)))

;; Enable projectile integration into helm.
(use-package helm-projectile
  :ensure t
  :config
  (helm-projectile-on))

(use-package helm-ag
  :ensure t)

(use-package helm-ros
  :ensure t
  :config
  (global-helm-ros-mode t))

;; Company stands for "COMPlete ANYthing". It is a pretty widely used
;; auto complete framework. Enable it globally.
(use-package company
  :ensure t
  :demand t
  :diminish company-mode
  :config
  (global-company-mode)
  :bind (:map company-active-map
              ("C-n" . company-select-next)
              ("C-p" . company-select-previous)
              ("C-d" . company-show-doc-buffer)
              ("M-." . company-show-location)))

;; Show popups with documentation strings where possible. The default
;; disables the auto pop up because it can cause some lag. Instead,
;; use M-h to show the help.
(use-package company-quickhelp
  :ensure t
  :config
  (company-quickhelp-mode 1)
  (setq company-quickhelp-delay nil))

;; magit.el is a git porcelain. It is actually a better git porcelain
;; than the git CLI itself. If you learn to use it, it will be your
;; friend. (https://www.emacswiki.org/emacs/Magit)
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)
         ("C-c g" . magit-status))
  :config
  (add-hook 'magit-mode-hook (lambda () (setq display-line-numbers nil))))

(use-package magit-lfs
  :ensure t)

;; Highlight symbols when pausing on them in any program mode.
;;
;; F3 goes to the next occurance of the symbol.
;; C-F3 locks highlighting so it stays highlighted when the point
;; leaves
;; Shift-F3 goes to the previous occurance of the symbol.
;; M-F3 replaces the symbol.
(use-package highlight-symbol
  :ensure t
  :init
  (add-hook 'prog-mode-hook 'highlight-symbol-mode)
  :diminish highlight-symbol-mode
  :bind (([(control f3)] . highlight-symbol)
         ([f3] . highlight-symbol-next)
         ([(shift f3)] . highlight-symbol-prev)
         ([(meta f3)] . highlight-symbol-query-replace)))

;; Flycheck.el is an automatic syntax checker/linter for many
;; languages. If you want to use it, it's worth running
;; `flycheck-verify-setup' in the buffer you want to use it on to see
;; if you have the preferred external programs installed. Windows
;; users, flycheck may not work so well for you as it is not
;; officially supported.
(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :config
  (defun flycheck-virtualenv-executable-find (executable)
    "Find an EXECUTABLE in the current virtualenv if any."
    (if (bound-and-true-p python-shell-virtualenv-root)
	(let ((exec-path (python-shell-calculate-exec-path)))
	  (executable-find executable))
      (executable-find executable)))

  (defun flycheck-virtualenv-setup ()
    "Setup Flycheck for the current virtualenv."
    (setq-local flycheck-executable-find #'flycheck-virtualenv-executable-find))
  (add-hook 'after-init-hook #'global-flycheck-mode)
  ;;(add-hook 'python-mode-hook #'flycheck-virtualenv-setup)
  )

;; Smartparens is a package that does its best job to make sure you
;; *never* insert unbalanced pairs into a buffer. It's a great
;; package, but does have quirks on non-sexp based languages
;; (*especially* when killing lines in Python). So, instead of
;; enabling it globally, we enable it only for modes where it makes
;; sense and works (like Lisps and web mode).
(use-package smartparens-config
  :ensure smartparens
  ;; We have to demand loading otherwise sometimes no configuration is
  ;; loaded.
  :demand t
  :diminish smartparens-strict-mode
  :diminish smartparens-mode
  :init
  (add-hook 'lisp-mode-hook 'turn-on-smartparens-strict-mode)
  (add-hook 'emacs-lisp-mode-hook 'turn-on-smartparens-strict-mode)
  (add-hook 'web-mode-hook 'turn-on-smartparens-strict-mode)
  (add-hook 'prog-mode-hook 'turn-on-smartparens-strict-mode)
  :config
  (setq sp-highlight-pair-overlay nil)
  (show-smartparens-global-mode t)
  ;; Disable the `' pair in docstrings for lisp. It makes it nearly
  ;; impossible to write doc strings using markdown.
  (sp-local-pair 'lisp-mode "`" nil :when '(sp-in-docstring-p) :actions nil)
  :bind (:map smartparens-mode-map
              (")" . sp-up-sexp)
              ("]" . sp-up-sexp)
              ("}" . sp-up-sexp)
              ("C-M-a" . sp-beginning-of-sexp)
              ("C-M-e" . sp-end-of-sexp)

              ("C-<down>" . sp-down-sexp)
              ("C-<up>"   . sp-up-sexp)
              ("M-<down>" . sp-backward-down-sexp)
              ("M-<up>"   . sp-backward-up-sexp)

              ("C-M-f" . sp-forward-sexp)
              ("C-M-b" . sp-backward-sexp)

              ("C-M-n" . sp-next-sexp)
              ("C-M-p" . sp-previous-sexp)

              ("C-S-f" . sp-forward-symbol)
              ("C-S-b" . sp-backward-symbol)

              ("C-<right>" . sp-forward-slurp-sexp)
              ("M-<left>" . sp-forward-barf-sexp)
              ("C-<left>"  . sp-backward-slurp-sexp)
              ("M-<right>"  . sp-backward-barf-sexp)

              ("C-M-t" . sp-transpose-sexp)
              ("C-M-k" . sp-kill-sexp)
              ;;("C-k"   . sp-kill-hybrid-sexp)
              ("M-k"   . sp-backward-kill-sexp)
              ("C-M-w" . sp-copy-sexp)

              ("M-s" . sp-splice-sexp)
              ("C-M-d" . delete-sexp)

              ("M-<backspace>" . backward-kill-word)
              ("C-<backspace>" . sp-backward-kill-word)
              ([remap sp-backward-kill-word] . backward-kill-word)

              ("C-x C-t" . sp-transpose-hybrid-sexp)

              ("C-c ("  . wrap-with-parens)
              ("C-c ["  . wrap-with-brackets)
              ("C-c {"  . wrap-with-braces)
              ("C-c '"  . wrap-with-single-quotes)
              ("C-c \"" . wrap-with-double-quotes)
              ("C-c _"  . wrap-with-underscores)
              ("C-c `"  . wrap-with-back-quotes)
              :map lisp-mode-map
              (";" . sp-comment)
              :map emacs-lisp-mode-map
              (";" . sp-comment)))

(use-package fill-column-indicator
  :ensure t
  :disabled t ;; Causes *huge* latency on scrolling
  :commands (fci-mode)
  :init
  (add-hook 'prog-mode-hook 'fci-mode)
  (add-hook 'web-mode-hook 'turn-off-fci-mode)
  (add-hook 'python-mode-hook (lambda () (setq-local fci-rule-column 100)))
  (add-hook 'lisp-mode-hook (lambda () (setq-local fci-rule-column 100)))
  (add-hook 'emacs-lisp-mode-hook (lambda () (setq-local fci-rule-column 100)))
  :config
  (setq-default fci-rule-column 80)
  (defvar-local company-fci-mode-on-p nil)

  (defun company-turn-off-fci (&rest ignore)
    (when (boundp 'fci-mode)
      (setq company-fci-mode-on-p fci-mode)
      (when fci-mode (fci-mode -1))))

  (defun company-maybe-turn-on-fci (&rest ignore)
    (when company-fci-mode-on-p (fci-mode 1)))

  (add-hook 'company-completion-started-hook 'company-turn-off-fci)
  (add-hook 'company-completion-finished-hook 'company-maybe-turn-on-fci)
  (add-hook 'company-completion-cancelled-hook 'company-maybe-turn-on-fci))

;; Always show column numbers on the modeline.
(setq column-number-mode t)

;; Display line numbers in prog-mode
(add-hook 'prog-mode-hook (lambda () (setq display-line-numbers t)))

;; Never, ever indent with tabs.
(setq-default indent-tabs-mode nil)

;; Put the scroll bars on the right.
(set-scroll-bar-mode 'right)

;; Make the cursor behave a little more intuitively when scrolling.
(setq scroll-preserve-screen-position t)

;; Make `tab-width' a little more sane (default is 8!)
(setq-default tab-width 4)


;;; Common Lisp configuration.

;; Useful for slime-repl-ansi-color contrib
(use-package ansi-color
  :ensure t)

;; The Superior Lisp Interaction Mode for Emacs.
(use-package slime
  :ensure t
  :defer t
  :diminish slime-mode
  :diminish eldoc-mode
  :commands (slime slime-connect)
  :config
  (require 'log4slime)
  (global-log4slime-mode 1)
  (slime-setup '(slime-fancy
                 slime-banner
                 slime-tramp
                 slime-repl-ansi-color
                 slime-company
                 slime-sprof))
  ;; Set the available lisp configurations.
  (setq slime-lisp-implementations
        `(;; SBCL can be a memory hog. Give it 6G of heap space.
          (sbcl ("sbcl" "--dynamic-space-size" "6144"))
          (ccl ("ccl"))
          (ecl ("ecl"))
          (alisp ("alisp"))
          (abcl ("abcl"))))
  ;; Default to SBCL
  (setq slime-default-lisp 'sbcl)
  ;; Useful function for killing SLIME
  (defun kill-slime ()
    (interactive)
    (when (slime-connected-p)
      (slime-quit-lisp)
      (slime-disconnect))
    (slime-kill-all-buffers))
  (add-hook 'slime-repl-mode-hook (lambda () (setq display-line-numbers nil)))
  (add-hook 'kill-emacs-hook #'kill-slime))

;; Provides highlighting of control sequences in format strings for
;; some common forms.
(use-package cl-format
  :ensure t
  :config
  (defconst cl-fontify-defforms-alist
    '((format . 2)
      (formatter . 1)
      (error . 1)
      (signal . 1)
      (warn . 1)
      (cerror . 1)
      (assert . 3)
      (invalid-method-error . 2)
      (method-combination-error . 2)
      (break . 1)
      (with-simple-restart . 2)
      (y-or-n-p . 1)))
  (defun fontify-control-strings ()
    (set
     (make-local-variable 'cl-format-fontify-defforms-alist)
     (append cl-format-fontify-defforms-alist
             cl-fontify-defforms-alist))
    (cl-format-font-lock-mode 1))
  (add-hook 'lisp-mode-hook 'fontify-control-strings))

;; Company integration for SLIME.
(use-package slime-company
  :ensure t
  :defer t)


;;; Configure slime-docker.

(use-package slime-docker
  :ensure t
  :commands slime-docker
  :config
  (let ((standard-options
         `(:rm t
               :image-name "registry.mers.csail.mit.edu/infrastructure/mtk-lisp-devel"
               :image-tag "latest"
               :mounts (((,(getenv "SSH_AUTH_SOCK") . "/tmp/S.gpg-agent.ssh"))
                        (("~/mtk/workspaces/" . "/home/lisp/workspaces/"))
                        (("~/mtk/lisp-scratch/" . "/scratch/"))
                        (("~/mtk/quicklisp/" . "/home/lisp/quicklisp/"))
                        (("~/.swank.lisp" . "/home/lisp/.swank.lisp"))
                        (("~/.cache/common-lisp/docker/" . "/home/lisp/.cache/common-lisp/")))
               :env (("LISP_DEVEL_UID" . "0")
                     ("SSH_AUTH_SOCK" . "/tmp/S.gpg-agent.ssh")))))
    (setq slime-docker-implementations
          `((sbcl ("sbcl" "--dynamic-space-size" "10144")
                  ,@standard-options
                  :security-opts (("seccomp" . ,slime-docker-sbcl-seccomp-profile)))
            (sbcl-testing ("sbcl" "--dynamic-space-size" "10144")
                          :image-tag "testing"
                          ,@standard-options
                          :security-opts (("seccomp" . ,slime-docker-sbcl-seccomp-profile)))
            (ccl ("ccl")
                 ,@standard-options)
            (ccl-testing ("ccl")
                         :image-tag "testing"
                         ,@standard-options)
            (alisp ("alisp")
                   ,@standard-options)
            (alisp-testing ("alisp")
                           :image-tag "testing"
                           ,@standard-options)
            (ecl ("ecl")
                 ,@standard-options)
            (ecl-testing ("ecl")
                         :image-tag "testing"
                         ,@standard-options)
            (abcl ("abcl")
                  ,@standard-options)
            (abcl-testing ("abcl")
                          :image-tag "testing"
                          ,@standard-options)))
    (setq slime-docker-default-lisp 'sbcl)))


;;; Python config.

(use-package elpy
  :ensure t
  :diminish t
  :config
  (add-hook 'elpy-mode-hook 'elpy-use-ipython)
  (defvar my-elpy-test-in-project-root)
  (make-variable-buffer-local 'my-elpy-test-in-project-root)
  (put 'my-elpy-test-in-project-root 'safe-local-variable #'booleanp)
  (define-advice elpy-test-at-point (:around (orig))
    (let ((results (funcall orig)))
      (when (bound-and-true-p my-elpy-test-in-project-root)
        (setq results (list* (elpy-project-root) (rest results))))
      results))
  (elpy-enable))

;; (use-package pycoverage
;;   :ensure t
;;   :diminish t
;;   :config
;;   (add-hook 'python-mode 'pycoverage-mode))

;; Jedi is a completion backend that company can use.
;; (use-package company-jedi
;;   :ensure t
;;   :config
;;   (add-to-list 'company-backends '(company-jedi company-files)))

;; Automatically activate virtualenvs.
;; (use-package auto-virtualenv
;;   :ensure t
;;   :commands (auto-virtualenv-set-virtualenv)
;;   :init
;;   (add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv))


;;; LaTeX config.

(use-package company-auctex
  :ensure t
  :commands company-auctex-init)

(use-package tex
  :ensure auctex
  :config
  (setq TeX-PDF-mode t
        TeX-source-correlate-mode t
        TeX-view-program-selection '(((output-dvi has-no-display-manager)
                                      "dvi2tty")
                                     ((output-dvi style-pstricks)
                                      "dvips and gv")
                                     (output-dvi "xdvi")
                                     (output-pdf "PDF Tools")
                                     (output-html "xdg-open")))
  (defun TeX-toggle-escape nil
    (interactive)
    "Toggle Shell Escape"
    (setq LaTeX-command
          (if (string= LaTeX-command "latex") "latex -shell-escape"
            "latex"))
    (message (concat "shell escape "
                     (if (string= LaTeX-command "latex -shell-escape")
                         "enabled"
                       "disabled"))))
  (add-hook 'LaTeX-mode-hook #'(lambda () (visual-line-mode 1)))
  (add-hook 'LaTeX-mode-hook #'(lambda () (flyspell-mode 1)))
  (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
  (company-auctex-init)
  :bind (:map LaTeX-mode-map
              ("C-c C-t x" . TeX-toggle-escape)))


;;; Miscellaneous modes.

(use-package json-mode
  :ensure t
  :mode "\\.json\\'")

(use-package dockerfile-mode
  :ensure t
  :mode "Dockerfile.*\\'")

(use-package cmake-mode
  :ensure t
  :mode ("\\.cmake\\'"
         "CMakeLists.txt"))

(use-package yaml-mode
  :ensure t
  :mode "\\.e?ya?ml\\'")

(use-package arduino-mode
  :ensure t
  :mode ("\\.ino\\'"
         "\\.pde\\'")
  :bind (:map arduino-mode-map
              ("C-c C-k" . compile)))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init
  (setq markdown-command "markdown2"))

(use-package web-mode
  :ensure t
  :mode ("\\.phtml\\'"
         "\\.tpl\\.php\\'"
         "\\.[agj]sp\\'"
         "\\.as[cp]x\\'"
         "\\.erb\\'"
         "\\.mustache\\'"
         "\\.djhtml\\'"
         "\\.html?\\'"
         "\\.xml\\'"
         "\\.launch\\'"))

(use-package protobuf-mode
  :ensure t
  :mode "\\.proto\\'")

(use-package flycheck-protobuf
  :ensure t
  :config
  (add-to-list 'flycheck-checkers 'protobuf-protoc-reporter t))



(use-package pdf-tools
  :ensure t
  :config
  (pdf-tools-install))



(use-package circe
  :ensure t
  :config
  (add-hook 'circe-chat-mode-hook 'my-circe-prompt)
  (defun my-circe-prompt ()
    (lui-set-prompt
     (concat (propertize (concat (buffer-name) ">")
                         'face 'circe-prompt-face)
             " ")))
  (setq lui-flyspell-p t
        lui-flyspell-alist '((".*" "american")))
  (defvar my-circe-bot-list '("Constable_Odo"))
  (defun my-circe-message-option-bot (nick &rest ignored)
    (when (member nick my-circe-bot-list)
      '((text-properties . (face circe-fool-face)))))
  (add-hook 'circe-message-option-functions 'my-circe-message-option-bot))

(use-package circe-notifications
  :ensure t
  :config
  (add-hook 'circe-server-connected-hook 'enable-circe-notifications))

(use-package helm-circe
  :ensure t
  :bind (("C-c c i" . helm-circe)
         ("C-c c n" . helm-circe-new-activity)))

(use-package alert
  :commands (alert)
  :ensure t
  :init
  (setq alert-default-style 'libnotify))

(use-package hydra
  :ensure t
  :config
  (defhydra hydra-projectile-other-window (:color teal)
    "projectile-other-window"
    ("f"  projectile-find-file-other-window        "file")
    ("g"  projectile-find-file-dwim-other-window   "file dwim")
    ("d"  projectile-find-dir-other-window         "dir")
    ("b"  projectile-switch-to-buffer-other-window "buffer")
    ("q"  nil                                      "cancel" :color blue))

  (defhydra hydra-projectile (:color teal
                                     :hint nil)
    "
     PROJECTILE: %(projectile-project-root)

     Find File            Search/Tags          Buffers                Cache
------------------------------------------------------------------------------------------
_s-f_: file            _a_: ag                _i_: Ibuffer           _c_: cache clear
 _ff_: file dwim       _g_: update gtags      _b_: switch to buffer  _x_: remove known project
 _fd_: file curr dir   _o_: multi-occur     _s-k_: Kill all buffers  _X_: cleanup non-existing
  _r_: recent file                                               ^^^^_z_: cache current
  _d_: dir

"
    ("a"   projectile-ag)
    ("b"   projectile-switch-to-buffer)
    ("c"   projectile-invalidate-cache)
    ("d"   projectile-find-dir)
    ("s-f" projectile-find-file)
    ("ff"  projectile-find-file-dwim)
    ("fd"  projectile-find-file-in-directory)
    ("g"   ggtags-update-tags)
    ("s-g" ggtags-update-tags)
    ("i"   projectile-ibuffer)
    ("K"   projectile-kill-buffers)
    ("s-k" projectile-kill-buffers)
    ("m"   projectile-multi-occur)
    ("o"   projectile-multi-occur)
    ("s-p" projectile-switch-project "switch project")
    ("p"   projectile-switch-project)
    ("s"   projectile-switch-project)
    ("r"   projectile-recentf)
    ("x"   projectile-remove-known-project)
    ("X"   projectile-cleanup-known-projects)
    ("z"   projectile-cache-current-file)
    ("`"   hydra-projectile-other-window/body "other window")
    ("q"   nil "cancel" :color blue))

  :bind (("C-c C-p" . hydra-projectile/body)))

;;; Smerge, originally from
;;; https://www.reddit.com/r/emacs/comments/6jhmbt/link_to_hydra_for_smergemode/
(use-package smerge-mode
  :bind (:map smerge-mode-map
              ("C-c m" . hydra-smerge/body))
  :init
  (progn
    (defun modi/enable-smerge-maybe ()
      "Auto-enable `smerge-mode' when merge conflict is detected."
      (save-excursion
        (goto-char (point-min))
        (when (re-search-forward "^<<<<<<< " nil :noerror)
          (smerge-mode 1))))
    (add-hook 'find-file-hook #'modi/enable-smerge-maybe :append))
  :config
  (when (< emacs-major-version 25)
    ;; http://git.savannah.gnu.org/cgit/emacs.git/commit/?id=bdfee01a6567b9f08f82bc84d1196e6cb62587ca
    (defalias 'smerge-keep-upper 'smerge-keep-mine)
    (defalias 'smerge-keep-lower 'smerge-keep-other)
    (defalias 'smerge-diff-base-upper 'smerge-diff-base-mine)
    (defalias 'smerge-diff-upper-lower 'smerge-diff-mine-other)
    (defalias 'smerge-diff-base-lower 'smerge-diff-base-other))

  (defhydra hydra-smerge (:color pink
                                 :hint nil
                                 :pre (smerge-mode 1)
                                 ;; Disable `smerge-mode' when quitting hydra if
                                 ;; no merge conflicts remain.
                                 :post (smerge-auto-leave))
    "
^Move^       ^Keep^               ^Diff^                 ^Other^
^^-----------^^-------------------^^---------------------^^-------
_n_ext       _b_ase               _<_: upper/base        _C_ombine
_p_rev       _u_pper              _=_: upper/lower       _r_esolve
^^           _l_ower              _>_: base/lower        _k_ill current
^^           _a_ll                _R_efine
^^           _RET_: current       _E_diff
"
    ("n" smerge-next)
    ("p" smerge-prev)
    ("b" smerge-keep-base)
    ("u" smerge-keep-upper)
    ("l" smerge-keep-lower)
    ("a" smerge-keep-all)
    ("RET" smerge-keep-current)
    ("\C-m" smerge-keep-current)
    ("<" smerge-diff-base-upper)
    ("=" smerge-diff-upper-lower)
    (">" smerge-diff-base-lower)
    ("R" smerge-refine)
    ("E" smerge-ediff)
    ("C" smerge-combine-with-next)
    ("r" smerge-resolve)
    ("k" smerge-kill-current)
    ("q" nil "cancel" :color blue)))



;; Pass integration

(use-package password-store
  :ensure t
  :config
  (setq password-store-password-length 16))

(use-package helm-pass
  :ensure t
  :commands (helm-pass))



;; Ansible

(use-package ansible
  :ensure t
  :config
  (setq ansible::vault-password-file "~/.local/bin/ansible-vault-password-mers")
  (add-hook 'ansible-hook (lambda () (add-hook 'hack-local-variables-hook
                                               'ansible::auto-decrypt-encrypt
                                               nil t)))
  (add-hook 'yaml-mode-hook '(lambda () (ansible 1))))

(use-package ansible-doc
  :ensure t
  :config
  (add-hook 'yaml-mode-hook #'ansible-doc-mode))

(use-package company-ansible
  :ensure t
  :config
  (add-to-list 'company-backends 'company-ansible))



;; Remove trailing whitespace

(add-hook 'before-save-hook (lambda ()
                              (unless (member major-mode '(markdown-mode))
                                (delete-trailing-whitespace))))



;; Reset GC threshold

(setq gc-cons-threshold my-original-gc-cons-threshold)

;;; Customization section. Please put any customizations you make
;;; after this line.  It makes it easier to merge changes.

;; This just provides a default location for you to put any
;; configuration you do not want tracked by git by default (such as
;; passwords, API tokens, etc.)
(defvar my-private-file "~/.emacs.d.priv/init.el")

(when (file-exists-p my-private-file)
  (load my-private-file))

(use-package persp-mode
  :ensure t
  :demand t
  :bind (("C-x x" . persp-key-map)))



(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(circe-originator-face ((t (:foreground "coral"))))
 '(font-lock-builtin-face ((t (:foreground "#009e73"))))
 '(line-number-current-line ((t (:inherit (default line-number))))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-save-file-name-transforms
   (quote
    (("\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'" "/tmp/\\2" t)
     (".*" "/home/etimmons/.emacs.d/auto-saves/" t))))
 '(backup-by-copying-when-linked t)
 '(backup-directory-alist (quote (("." . "/home/etimmons/.emacs.d/backups"))))
 '(circe-notifications-watch-strings
   (quote
    ("etimmons/mers-slack" "admin" "etimmons/mers-slack" "admin" "@etimmons" "etimmons" "etimmons/mers-slack" "admin" "Eric Timmons")))
 '(delete-auto-save-files nil)
 '(delete-old-versions t)
 '(display-line-numbers-width 4)
 '(elpy-dedicated-shells nil)
 '(elpy-project-root-finder-functions
   (quote
    (elpy-project-find-python-root elpy-project-find-git-root elpy-project-find-hg-root elpy-project-find-svn-root)))
 '(fci-rule-use-dashes t)
 '(fill-column 80)
 '(glasses-face (quote bold-italic))
 '(helm-swoop-speed-or-color t)
 '(helm-swoop-use-fuzzy-match nil)
 '(inhibit-startup-screen t)
 '(kept-new-versions 10)
 '(kept-old-versions 5)
 '(notmuch-saved-searches
   (quote
    ((:name "inbox" :query "tag:inbox" :key "i")
     (:name "unread" :query "tag:unread" :key "u")
     (:name "flagged" :query "tag:flagged" :key "f")
     (:name "sent" :query "tag:sent" :key "t")
     (:name "drafts" :query "tag:draft" :key "d")
     (:name "all mail" :query "*" :key "a")
     (:name "recent" :query "tag:inbox and tag:recent"))))
 '(notmuch-search-oldest-first nil)
 '(package-selected-packages
   (quote
    (beacon sphinx-doc ansible company-ansible ein minizinc-mode helm-ag perspective helm-gitlab matlab-mode color-theme-solarized color-theme flycheck-protobuf protobuf-mode web-mode markdown-mode arduino-mode yaml-mode dockerfile-mode company-auctex company-jedi slime-docker slime-company cl-format slime smartparens flycheck highlight-symbol magit company-quickhelp company helm-projectile projectile form-feed hl-todo editorconfig smooth-scrolling swiper-helm spaceline helm ace-window avy sr-speedbar neotree which-key use-package)))
 '(python-shell-interpreter "ipython")
 '(python-shell-interpreter-args "-i --nosep --pdb")
 '(safe-local-variable-values
   (quote
    ((ansible::vault-password-file . "/home/etimmons/.local/bin/ansible-vault-password-mers-intro-to-autonomy-fa17")
     (Package . ILP)
     (Package . COMMON-LISP-USER)
     (Package . OPSAT)
     (Base . 10)
     (Package . CS))))
 '(scroll-conservatively 0)
 '(switch-to-buffer-preserve-window-point nil)
 '(version-control t))

;;; init.el ends here
(put 'narrow-to-region 'disabled nil)
