# MERS Emacs Config #

This repo contains a basic working Emacs configuration that sets up
Emacs for a variety of languages and purposes. Detailed documentation
is kept up to date in the `init.el` file under the "Commentary:"
section. Please read that for full information on what this
configuration does.

## Installation ##

To install this config, first remove any current Emacs config you have
at `~/.emacs` and `~/.emacs.d`.

After removing any existing configs, clone this repo as `~/.emacs.d`.

Then, start Emacs and wait. On the first run, it fetches all the
necessary packages and installs them.

## Notes ##

### Windows users ###

Unless you have configured the `HOME` environment variable, Emacs sees
your `~` folder as `C:\Users\USERNAME\AppData\Roaming`.

You will also need to install gnutls so Emacs can make a secure
connection to MELPA. Download the gnutls binaries from
http://sourceforge.net/projects/ezwinports/files/ and unzip them to
the same folder where you have emacs installed.
