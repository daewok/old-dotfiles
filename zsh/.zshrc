# Path to your oh-my-zsh installation.
export ZSH=$HOME/oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="gentoo"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias cp='cp --reflink=auto'

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to disable command auto-correction.
# DISABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

if [[ -z $ZSH_TMUX_AUTOSTART ]]; then
    ZSH_TMUX_AUTOSTART="true"
fi

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=~/.oh-my-zsh.custom

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git lisp taskwarrior svn tmux pass heroku)

# ZSH_TMUX_AUTOSTART=true
source $ZSH/oh-my-zsh.sh

# User configuration

# export PATH=$HOME/bin:/usr/local/bin:$PATH:$HOME/.local/bin:$HOME/.rvm/bin:/usr/local/heroku/bin
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

function load_rvm {
  export PATH=$HOME/.rvm/bin:$PATH
  source ~/.rvm/scripts/rvm
  export PATH="$(ruby -rubygems -e 'puts Gem.user_dir')/bin:$PATH"
}

# source ~/.rvm/scripts/rvm

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

#source ~/mtk/crash-course/setup.sh

source /usr/bin/virtualenvwrapper.sh

export ARDUINO_DIR=/opt/arduino-1.6.5-r5
export ARDMK_DIR=/opt/Arduino-Makefile
export AVR_TOOLS_DIR=/usr
export AVRDUDE=/usr/bin/avrdude
export AVRDUDE_CONF=/etc/avrdude.conf

export PATH=~/.local/bin:$PATH

export ANSIBLE_VAULT_PASSWORD_FILE=~/.local/bin/ansible-vault-password-mers

function load_ros {
    source ~/catkin_ws/devel/setup.zsh
    #export ROS_MASTER_URI=http://simstick.robocup-logistics.org:11311/
    #export ROS_IP=192.168.56.1
}

# if [ -e ~/catkin_ws/install/setup.zsh ]; then
#     source ~/catkin_ws/install/setup.zsh
# fi

function xwam {
  export ROS_MASTER_URI=http://mers-loki-xwam:11311/
  export ROS_IP=192.168.2.110
}

#export TERM=rxvt-unicode-256color

# User specific aliases and functions
# export FAWKES_DIR=~/robotics/fawkes-robotino
# export GAZEBO_RCLL=~/robotics/gazebo-rcll
# export GAZEBO_PLUGIN_PATH=$GAZEBO_PLUGIN_PATH:$GAZEBO_RCLL/plugins/lib/gazebo
# export GAZEBO_MODEL_PATH=$GAZEBO_RCLL/models
# export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:$GAZEBO_RCLL/models/carologistics

# export LLSF_REFBOX_DIR=~/robotics/llsf-refbox
# export GAZEBO_WORLD_PATH=$GAZEBO_RCLL/worlds/carologistics/llsf.world

# source /usr/share/gazebo/setup.sh
# source $HOME/robotics/ros/catkin_ws_kinetic/devel/setup.sh

alias ls='ls --color=tty --quoting-style=literal'

unalias sudo 2> /dev/null
